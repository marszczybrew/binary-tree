## Drzewo poszukiwań binarnych
[zapraszam do wyświetlania na GitLabie](https://gitlab.com/marszczybrew/binary-tree)

>W pliku [Leaf.php](src/Leaf.php) od linii 104 jest zaimplementowany algorytm przechodzenia drzewa w poziomie

#### Definicja
Binarne drzewo poszukiwań - dynamiczna struktura danych będąca drzewem binarnym, w którym lewe poddrzewo każdego węzła zawiera wyłącznie elementy o kluczach nie większych niż klucz węzła a prawe poddrzewo zawiera wyłącznie elementy o kluczach nie mniejszych niż klucz węzła. Węzły, oprócz klucza, przechowują wskaźniki na swojego lewego i prawego syna oraz na swojego ojca.
                            
Koszt wykonania podstawowych operacji w drzewie BST (wstawienie, wyszukanie, usunięcie węzła) jest proporcjonalny do wysokości drzewa h, ponieważ operacje wykonywane są wzdłuż drzewa. Fakt ten w notacji Landaua zapisuje się O(h). Jeżeli drzewo jest zrównoważone, to jego wysokość bliska jest logarytmowi dwójkowemu liczby węzłów, zatem dla drzewa o n węzłach optymistyczny koszt każdej z podstawowych operacji wynosi O(log n). Z drugiej strony drzewo skrajnie niezrównoważone ma wysokość porównywalną z liczbą węzłów (w skrajnym przypadku drzewa zdegenerowanego do listy wartości te są równe: h=n), z tego powodu koszt pesymistyczny wzrasta do O(n).

#### Zakres implementacji
W ramach zadania zaimplementowane zostały funkcje pozwalające na:
 - wstawienie elementu do drzewa na odpowiednią pozycję (iteracyjnie jak i przy wykorzystaniu rekurencji),
 - sprawdzenie czy element znajduje się w drzewie (także w dwóch wersjach),
 - sprawdzenie czy drzewo jest zbalansowane,
 - zbalansowanie drzewa
 
#### Złożoność czasowa
Poniższy wykres przedstawia złożoność czasową operacji dodawania elementów do drzewa oraz jego balansowania (po wstawieniu wszystkich elementów w danym przebiegu drzewo było balansowane). Elementy były w losowej kolejności.
 ![czasowa.png](czasowa.png)
 
#### Złożoność obliczeniowa
W przypadku gdy drzewo jest zbalansowane wyszukanie/dodanie elementy wymaga wykonania log2(n) operacji, gdzie n to liczba elementów już w nim obecnych. W przypadku degeneracji drzewa złożoność rośnie do maksymalnie n operacji, czyli do momentu gdy drzewo zdegeneruje się do listy liniowej.
 
#### Przywracanie drzewa do zbalansowanego stanu
Operacja balansowania drzewa została zaimplementowana w poniższy sposób:
 - pobierz wszystkie dane z drzewa do tymczasowej tablicy,
 - posortuj tablicę stworzonym na poprzednich zajęciach algorytmem merge-sort,
 - metodą podobną do bisekcji od nowa wstaw elementy do drzewa.
   
Metoda podobna do bisekcji polega na wstawieniu elementu środkowego, następnie dwóch elementów środkowych w powstałych połówkach, itd. Taki sposób postępowania zapewnia, że drzewo się nie zdegeneruje.