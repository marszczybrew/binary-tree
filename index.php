<?php
/**
 * Created by PhpStorm.
 * User: stud
 * Date: 24.10.16
 * Time: 08:41
 */

define('MAX', 600);
define('MIDDLE', 3000);

use Tree\Car;
use Tree\Leaf;

require __DIR__ . "/vendor/autoload.php";

function test(callable $test):string
{
    $start = microtime(true);
    $test();
    $end = microtime(true);

//    var_dump(str_replace('.', ',',$end - $start));
    return str_replace('.', ',', $end - $start);
}

echo "<table><tbody>";
for ($i = 200; $i <= MAX; $i += 200) {
    $test    = new Leaf(new Car($i / 2));
    $numbers = range(0, $i);
    for ($j = 0; $j < $i / 100; $j++) {
        $randoms[] = random_int(0, $i);
    }
    shuffle($numbers);

    $add = test(function () use (&$test, $numbers, $i) {
        foreach ($numbers as $number) {
            if ($number === $i / 2) {
                continue;
            }
            $test->addIterative(new Car($number));
        }
    });

    $sort = test(function () use (&$test) { $test->balance(); });

    $searchIterative = test(function () use (&$test, $randoms) {
        foreach ($randoms as $random) {
            $test->searchIterative($random);
        }
    });

    $searchRecursive = test(function () use (&$test, $randoms) {
        foreach ($randoms as $random) {
            $test->search($random);
        }
    });


    echo "<tr><td>$i</td><td>$add</td><td>$sort</td><td>$searchIterative</td><td>$searchRecursive</td></tr>";
}
echo "</tbody></table>";