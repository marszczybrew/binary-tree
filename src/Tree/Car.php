<?php
/**
 * Created by PhpStorm.
 * User: stud
 * Date: 24.10.16
 * Time: 08:43
 */

namespace Tree;


class Car implements Comparable
{
    /** @var int */
    private $power;

    public function __construct(int $power)
    {
        $this->power = $power;
    }

    public function compareTo($o)
    {
        if ($o instanceof $this) {
            if ($o->getPower() === $this->getPower())
                return 0;
            else
                return ($o->getPower() > $this->getPower());
        }
        return 1;
    }

    /**
     * @return int
     */
    public function getPower()
    {
        return $this->power;
    }

    /**
     * @param int $power
     */
    public function setPower(int $power)
    {
        $this->power = $power;
    }

    public function getValue()
    {
        return $this->getPower();
    }
}