<?php
/**
 * Created by PhpStorm.
 * User: stud
 * Date: 24.10.16
 * Time: 08:42
 */

namespace Tree;


interface Comparable
{
    public function compareTo($o);

    public function getValue();
}