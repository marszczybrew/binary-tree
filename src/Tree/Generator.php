<?php
/**
 * Created by PhpStorm.
 * User: michal.markiewicz
 * Date: 06.11.2016
 * Time: 21:23
 */

namespace Tree;


class Generator
{
    public static function create(int $depth)
    {
        $count = pow(2, $depth) - 1;
        $root = new Leaf(new Car(round($count / 2)));
        $level = 1;
        for ($i = (int)round($count / 2); $i > 1; $i /= 2) {
            $per_level = pow(2, $level) - 1;
            for ($j = (int)round($i / 2); $j < $per_level; $j += $i) {
                $root->addIterative(new Car($j));
            }
            $level++;
        }
    }
}