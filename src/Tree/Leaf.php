<?php

namespace Tree;

/**
 * Created by PhpStorm.
 * User: stud
 * Date: 24.10.16
 * Time: 08:39
 */
class Leaf
{
    //region Fields and constructor
    /** @var Leaf */
    private $left;
    /** @var  Leaf */
    private $right;
    /** @var  Comparable */
    private $data;

    /**
     * Leaf constructor.
     *
     * @param Comparable $data
     */
    public function __construct(Comparable $data)
    {
        $this->data  = $data;
        $this->left  = null;
        $this->right = null;
    }
    //endregion

    //region has- functions

    public function hasChildren(): bool
    {
        return ($this->hasLeft() || $this->hasRight());
    }

    public function hasLeft() : bool
    {
        return $this->left !== null;
    }

    public function hasRight(): bool
    {
        return $this->right !== null;
    }

    //endregion

    public function addIterative(Comparable $item):Leaf
    {
        $node = $this;
        while (true) {
            $dir = 'Left';
            if ($item->compareTo($node->data) <= 0) {
                $dir = 'Right';
            }
            $set_method = 'set' . $dir;
            $has_method = 'has' . $dir;
            $get_method = 'get' . $dir;
            if ($node->$has_method()) {
                $node = $node->$get_method();
                continue;
            }

            return $node->$set_method(new Leaf($item));
        }
    }

    public function search($value): bool
    {
        if ($this->data->getValue() === $value) {
            return true;
        } elseif ($this->hasLeft() && $this->data->getValue() > $value) {
            return $this->left->search($value);
        } elseif ($this->hasRight() && $this->data->getValue() < $value) {
            return $this->right->search($value);
        } else {
            return false;
        }
    }

    //region Search functions

    public function searchIterative($value): bool
    {
        $item = $this;
        while ($item->getData()->getValue() !== $value) {
            if ($item->hasLeft() && $item->data->getValue() > $value) {
                $item = $item->getLeft();
            } elseif ($item->hasRight() && $item->data->getValue() < $value) {
                $item = $item->getRight();
            } else {
                break;
            }
        }

        return $item->getData()->getValue() === $value;
    }

    public function bfSearch($search)
    {
        /** @var Leaf[] $parents */
        $parents[] = $this;

        while (count($parents) > 0) {
            /** @var Leaf[] $childrens */
            $childrens = [];

            foreach ($parents as $parent) {
                if ($parent->getData()->getValue() === $search)
                    return $parent;
                if ($parent->hasLeft())
                    $childrens[] = $parent->getLeft();
                if ($parent->hasRight())
                    $childrens[] = $parent->getRight();
            }

            $parents = $childrens;
        }
    }

    /**
     * @return Comparable
     */
    public function getData(): Comparable
    {
        return $this->data;
    }
    //endregion
    //region Getters and setters

    /**
     * @param Comparable $data
     *
     * @return Leaf
     */
    public function setData(Comparable $data): Leaf
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return Leaf
     */
    public function getLeft(): Leaf
    {
        return $this->left;
    }

    /**
     * @param Leaf $left
     *
     * @return Leaf
     */
    public function setLeft(Leaf $left): Leaf
    {
        $this->left = $left;

        return $this->left;
    }

    /**
     * @return Leaf
     */
    public function getRight(): Leaf
    {
        return $this->right;
    }

    /**
     * @param Leaf $right
     *
     * @return Leaf
     */
    public function setRight(Leaf $right): Leaf
    {
        $this->right = $right;

        return $this->right;
    }

    public function isBalanced():bool
    {
        return $this->checkBalance($this) !== -1;
    }

    public function balance()
    {
        $items = $this->getDataRecursive();
        Sorter::sort($items);
        $this->removeData();
        $level = 1;
        for ($i = (int) round(count($items) / 2); $i > 1; $i /= 2) {
            $per_level = pow(2, $level) - 1;
            for ($j = (int) round($i / 2); $j < $per_level; $j += $i) {
                $this->add($items[$j]);
            }
            $level++;
        }
    }

    //endregion
    //region Balance checks

    /**
     * @return Comparable[]
     */
    public function getDataRecursive():array
    {
        $children   = [];
        $children[] = $this->getData();
        if ($this->hasLeft()) {
            $children = array_merge($children, $this->getLeft()
                                                    ->getDataRecursive());
        }
        if ($this->hasRight()) {
            $children = array_merge($children, $this->getRight()
                                                    ->getDataRecursive());
        }

        return $children;
    }

    /**
     * @param Comparable $item
     *
     * @return Leaf
     */
    public function add(Comparable $item) :Leaf
    {
        $dir = 'Left';
        if ($item->compareTo($this->data) <= 0) {
            $dir = 'Right';
        }
        $set_method = 'set' . $dir;
        $has_method = 'has' . $dir;
        $get_method = 'get' . $dir;
        if ($this->$has_method()) {
            return $this->$get_method()->add($item);
        }

        return $this->$set_method(new Leaf($item));
    }

    //endregion

    private function checkBalance(Leaf $node):int
    {
        if ($node === null) {
            return 0;
        }

        $left = ($node->hasLeft() ? $this->checkBalance($node->getLeft()) : 0);
        if ($left === -1) {
            return -1;
        }

        $right = ($node->hasRight() ? $this->checkBalance($node->getRight()) : 0);
        if ($right === -1) {
            return -1;
        }

        if (abs($left - $right) > 1) {
            return -1;
        } else {
            return 1 + max($left, $right);
        }
    }

    private function removeData()
    {
        $this->left  = null;
        $this->right = null;
        $this->data  = null;
    }
}